using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] TMP_Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = $"Score: {CalculateScore()}";
    }
    public int CalculateScore()
    {
        int score = 0;
        foreach (var ball in BallsManager.Instance.balls)
        {
            if(ball) score += ball.GetScore();
        }
        return score;
    }
}

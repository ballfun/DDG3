using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _target;

    [SerializeField] private bool _invertVertical = true;
    [SerializeField] private float _horizontalSpeed = 2.0f;
    [SerializeField] private float _verticalSpeed = 2.0f;

    [SerializeField] private float _maxVerticalAngle = 80.0f;
    [SerializeField] private float _minVerticalAngle = 0f;

    [SerializeField] private float _armLength = 5.0f;

    [SerializeField] private float _currentHorizontalAngle = 0.0f;
    [SerializeField] private float _currentVerticalAngle = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //set mouse mode to lock
        Cursor.lockState = CursorLockMode.Locked;
        _currentHorizontalAngle += _horizontalSpeed * Input.GetAxis("Mouse X") * Time.deltaTime;
        _currentHorizontalAngle = Mathf.Repeat(_currentHorizontalAngle, 360.0f);
        _currentVerticalAngle += _verticalSpeed * Input.GetAxis("Mouse Y") * Time.deltaTime * (_invertVertical ? -1 : 1);
        _currentVerticalAngle = Mathf.Clamp(_currentVerticalAngle, _minVerticalAngle, _maxVerticalAngle);

        transform.rotation = Quaternion.Euler(_currentVerticalAngle , _currentHorizontalAngle, 0.0f);
        var finalArmLength = _armLength;
        RaycastHit hit;
        if (Physics.Raycast(_target.position, -transform.forward, out hit, _armLength))
        {
            finalArmLength = hit.distance;
        }
        transform.position = _target.position - transform.forward * finalArmLength;
    }
}

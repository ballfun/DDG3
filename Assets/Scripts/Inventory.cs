
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [Header("Inventory")]
    [SerializeField] List<Item> inventory = new List<Item>();
    [Header("Internal")]
    [SerializeField] List<Item> items = new List<Item>();
    public int addIndex = 0;
    [Button]
    public void AddItem()
    {
        inventory.Add(items[addIndex]);
    }
    [Button]
    public void UseItem()
    {
        Debug.Log(inventory[0].itemDescription);
        inventory.RemoveAt(0);
    }
}

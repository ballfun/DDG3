using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class BallsManager : MonoBehaviour
{
    public static BallsManager Instance;
    [SerializeField] Transform arrow;
    [AssetsOnly] [SerializeField] BallController ballPrefab;
    [SerializeField] BallController activeBall;
    [HideInInspector] public List<BallController> balls = new List<BallController>();

    [Header("Shoot Settings")] [SerializeField]
    float rotLimit = 45f;

    [SerializeField] float posLimit = 5f;
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float rotSpeed = 5f;
    [SerializeField] float shootForceSpeed = 5f;
    [SerializeField] float shootForceMin = 5f;
    [SerializeField] float shootForceMax = 20f;

    private float _currentRot;
    private float _currentPower;
    private GameStates _currentState;
    private float _timer;

    private bool _swingDir;
    private bool _posDir;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        _currentState = GameStates.Setup;
    }


    // Update is called once per frame
    void Update()
    {
        switch (_currentState)
        {
            case GameStates.Idle:

                break;
            case GameStates.Setup:
                var b = Instantiate(ballPrefab, transform.position + new Vector3(0, 0.5f, 0), Quaternion.identity);
                activeBall = b;
                balls.Add(b);
                b.Rb.isKinematic = true;
                _currentState = GameStates.Aiming;
                break;
            case GameStates.Aiming:
                arrow.gameObject.SetActive(true);
                arrow.localScale = Vector3.one;
                arrow.position = activeBall.transform.position;
                _currentRot += rotSpeed * Time.deltaTime * (_swingDir ? 1 : -1);
                if (_currentRot > rotLimit)
                {
                    _swingDir = false;
                }
                else if (_currentRot < -rotLimit)
                {
                    _swingDir = true;
                }

                arrow.rotation = Quaternion.Euler(0, _currentRot, 0);
                float h = Input.GetAxis("Horizontal");
                activeBall.transform.position =
                    new Vector3(
                        Mathf.Clamp(activeBall.transform.position.x + h * moveSpeed * Time.deltaTime, -posLimit,
                            posLimit),
                        activeBall.transform.position.y, activeBall.transform.position.z);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    _currentPower = Random.Range(shootForceMin, shootForceMax);
                    _currentState = GameStates.Shooting;
                }

                break;
            case GameStates.Shooting:
                _currentPower += shootForceSpeed * Time.deltaTime * (_posDir ? 1 : -1);
                if (_currentPower > shootForceMax || _currentPower < shootForceMin)
                {
                    _posDir = !_posDir;
                }
                arrow.localScale = new Vector3(1, 1, _currentPower / shootForceMax);
                if (Input.GetKeyUp(KeyCode.Space))
                {
                    activeBall.Shoot(arrow.forward, _currentPower);
                    _currentState = GameStates.Awaiting;
                    _timer = 0.5f;
                }

                break;
            case GameStates.Awaiting:
                arrow.gameObject.SetActive(false);
                bool ready = false;
                if (activeBall != null)
                {
                    ready = activeBall.Rb.velocity.magnitude < 0.1f;
                }
                else
                {
                    ready = true;
                }

                if (_timer > 0) ready = false;
                if (ready)
                {
                    _currentState = GameStates.Setup;
                    activeBall = null;
                }

                _timer -= Time.deltaTime;
                break;
        }
    }

    public enum GameStates
    {
        Idle,
        Setup,
        Aiming,
        Shooting,
        Awaiting
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    private Collider _scoreZone;
    public Rigidbody Rb => rb;

    public void Shoot(Vector3 dir, float force)
    {
        rb.isKinematic = false;
        rb.AddForceAtPosition(dir * force, transform.position + new Vector3(0, .3f, 0), ForceMode.Impulse);
    }

    private void Update()
    {
        if (transform.position.y < -10)
            Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("ScoreZone"))
        {
            _scoreZone = other;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other==_scoreZone) _scoreZone = null;
    }

    public int GetScore()
    {
        if (_scoreZone == null) return 0;
        int score = 0;
        if (_scoreZone.name.StartsWith("ScoreZone_"))
        {
            string[] sp = _scoreZone.name.Split('_');
            if (sp.Length < 2) return 0;
            int.TryParse(sp[1], out score);
        }
        return score;
    }

    private void OnDestroy()
    {
        if(BallsManager.Instance.balls.Contains(this)) BallsManager.Instance.balls.Remove(this);
    }
}
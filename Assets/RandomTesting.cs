using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTesting : MonoBehaviour
{
    [SerializeField] List<RandomString> ranString = new List<RandomString>();
    [SerializeField] int[] count = new int[5];
    // Start is called before the first frame update
    void Start()
    {
        count = new int[ranString.Count];
        float totalWeight = 0;
        for (int i = 0; i < ranString.Count; i++)
        {
            totalWeight += ranString[i].weight;
        }
        for (int i = 0; i < 1000; i++)
        {
            if(totalWeight<=0) continue;
            float ran = Random.Range(0, totalWeight);
            int ranIndex = -1;
            for (int j = 0; j < ranString.Count; j++)
            {
                ran -= ranString[j].weight;
                if (ran <= 0)
                {
                    ranIndex = j;
                    break;
                }
            }
            
            Debug.Log(ranString[ranIndex].name);
            count[ranIndex]++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
}
[System.Serializable]
public class RandomString
{
    public string name;
    public float weight = 1;
}
